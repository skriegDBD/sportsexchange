// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
//import BootstrapVue from 'bootstrap-vue'
import axios from 'axios'
import VueCookies from 'vue-cookies'
import Buefy from 'buefy'
import Toasted from 'vue-toasted';
import Multiselect from 'vue-multiselect';

import {store} from './store';

//Vue.use(BootstrapVue);
Vue.use(Buefy);
Vue.use(VueCookies);
Vue.use(Toasted, {
	router
});
Vue.use(require('vue-moment'));

Vue.component('multiselect', Multiselect);

Vue.prototype.$http = axios;

Vue.config.productionTip = false;

Vue.filter('currencyDisplay', function (value) {
  return '$'+parseFloat(value)
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
