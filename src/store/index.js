import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    tags: []
  },
  mutations: {
    addTag(state, tag) {
    	state.tags.push(tag);
    },
    removeTag(state, tag) {
    	state.tags = state.tags.filter(e => e !== tag);
    },
    updateTags(state, tags) {
    	state.tags = tags;
	}
  },
  getters: {
    getTags: state => state.tags
  }
})