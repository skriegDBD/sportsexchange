import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import MainPage from '@/components/MainPage'
import EventPage from '@/components/EventPage'
import MatchPage from '@/components/MatchPage'
import MyEventHistory from '@/components/MyEventHistory'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MainPage',
      component: MainPage
    },
    {
      path: '/myevents',
      name: "MyEvents",
      component: MyEventHistory
    },
    {
      path: '/event/:id',
      name: 'event',
      component: EventPage
    },
    {
      path: '/match/:id',
      name: 'match',
      component: MatchPage
    },
    {
      path: '/:tag',
      name: 'MainPageTags',
      component: MainPage
    }
  ]
});